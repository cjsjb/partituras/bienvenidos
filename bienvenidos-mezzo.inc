\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key a \major

		R1*4  |
%% 5
		e' 4 e' 8 e' 4 e' 4.  |
		R1  |
		e' 4 e' 8 e' 4 e' 4. (  |
		d' 4. b ) r4  |
		e' 4 e' 8 e' 4 e' 4.  |
%% 10
		R1  |
		e' 4 e' 8 e' 4 e' 4. (  |
		d' 4. b ) r4  |
		d' 2. d' 4  |
		e' 4. e' 4 e' 8 e' 4  |
%% 15
		gis' 1 (  |
		a' 2 ) r4 cis'  |
		d' 2. d' 4  |
		b 2. b 4  |
		cis' 1  |
%% 20
		r2 r4 cis'  |
		d' 4. fis' 8 ~ fis' 2  |
		r2 gis' 4 gis'  |
		gis' 1 (  |
		a' 2. ) cis' 8 d'  |
%% 25
		d' 4. d' d' 4  |
		e' 4 e' e' e'  |
		cis' 2. r4  |
		r2. cis' 8 d'  |
		d' 4. d' d' 4  |
%% 30
		e' 4 e' e' e'  |
		cis' 2. r4  |
		r2 r4 cis' 8 d'  |
		d' 4. d' d' 4  |
		e' 4 e' e' e'  |
%% 35
		cis' 1  |
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "mezzo" {
		Bien -- ve -- ni -- dos, bien -- ve -- ni -- dos. __
		Bien -- ve -- ni -- dos, bien -- ve -- ni -- dos. __

		Hoy reu -- ni -- dos en la fe __
		po -- de -- mos dar a -- mor,
		"e i" -- re -- mos por do -- quier __
		y se -- re -- mos el fru -- to del Se -- ñor,
		y se -- re -- mos el fru -- to del Se -- ñor,
		y se -- re -- mos el fru -- to del Se -- ñor.
	}
>>
