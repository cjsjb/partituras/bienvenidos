\context ChordNames
	\chords {
	\set chordChanges = ##t

	% intro
	a1 d4. e8 ~ e2
	a1 d4. e8 ~ e2

	% bienvenidoos...
	a1 d4. e8 ~ e2
	a1 d4. e8 ~ e2

	% bienvenidoos...
	a1 d4. e8 ~ e2
	a1 d4. e8 ~ e2

	% hoooy, reuniiidos...
	d1 e1 cis1:m fis1:m
	d1 e1 a1 a1:7
	d1 e1 cis1:m fis1:m

	d1 e1 a1 fis1:m
	d1 e1 a1 fis1:m
	d1 e1 a1 d4. e8 ~ e2
	a1
	}
