\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble_8"
		\key a \major

		R1*4  |
%% 5
		e 4 e 8 e 4 e 4.  |
		R1  |
		e 4 e 8 e 4 e 4. (  |
		fis 4. gis ) r4  |
		e 4 e 8 e 4 e 4.  |
%% 10
		R1  |
		e 4 e 8 e 4 e 4. (  |
		fis 4. gis ) r4  |
		d 2. a 4  |
		gis 4. fis 4 e 8 fis 4  |
%% 15
		e 1 (  |
		fis 2 ) r4 cis  |
		d 2. fis 4  |
		e 2. d 4  |
		e 1  |
%% 20
		r2 r4 cis  |
		d 4. fis 8 ~ fis 2  |
		r2 gis 4 b  |
		e 1 (  |
		fis 2. ) fis 8 gis  |
%% 25
		a 4. fis gis 4  |
		a 4 gis fis gis  |
		a 2. r4  |
		r2. fis 8 gis  |
		a 4. fis gis 4  |
%% 30
		a 4 gis fis gis  |
		a 2. r4  |
		r2 r4 fis 8 gis  |
		a 4. fis gis 4  |
		a 4 gis fis gis  |
%% 35
		a 1  |
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "tenor" {
		Bien -- ve -- ni -- dos, bien -- ve -- ni -- dos. __
		Bien -- ve -- ni -- dos, bien -- ve -- ni -- dos. __

		Hoy reu -- ni -- dos en la fe __
		po -- de -- mos dar a -- mor,
		"e i" -- re -- mos por do -- quier __
		y se -- re -- mos el fru -- to del Se -- ñor,
		y se -- re -- mos el fru -- to del Se -- ñor,
		y se -- re -- mos el fru -- to del Se -- ñor.
	}
>>
