\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key a \major

		R1*4  |
%% 5
		e' 4 e' 8 e' 4 e' 4.  |
		r8 a' 8. gis' fis' fis' gis' 8 ~  |
		gis' 8. e' 16 ~ e' 2.  |
		R1  |
		e' 4 e' 8 e' 4 e' 4.  |
%% 10
		r8 a' 8. gis' fis' fis' gis' 8 ~  |
		gis' 8. e' 16 ~ e' 2.  |
		R1  |
		d' 2. a' 4  |
		gis' 4. fis' 4 e' 8 fis' 4  |
%% 15
		e' 1 (  |
		fis' 2 ) r4 cis'  |
		d' 2. fis' 4  |
		e' 2. d' 4  |
		e' 1  |
%% 20
		r2. cis' 4  |
		d' 4. fis' 8 ~ fis' 2  |
		r2 gis' 4 b'  |
		e' 1 (  |
		fis' 2. ) fis' 8 gis'  |
%% 25
		a' 4. fis' gis' 4  |
		a' 4 gis' fis' gis'  |
		a' 2. r4  |
		r2. fis' 8 gis'  |
		a' 4. fis' gis' 4  |
%% 30
		a' 4 gis' fis' gis'  |
		a' 2. r4  |
		r2 r4 fis' 8 gis'  |
		a' 4. fis' gis' 4  |
		a' 4 gis' fis' gis'  |
%% 35
		a' 1  |
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "soprano" {
		Bien -- ve -- ni -- dos don -- "de ex" -- is -- "te el" a -- mor.
		Bien -- ve -- ni -- dos don -- de vi -- "ve el" Se -- ñor.

		Hoy reu -- ni -- dos en la fe
		po -- de -- mos dar a -- mor,
		"e i" -- re -- mos por do -- quier __
		y se -- re -- mos el fru -- to del Se -- ñor,
		y se -- re -- mos el fru -- to del Se -- ñor,
		y se -- re -- mos el fru -- to del Se -- ñor.
	}
>>
